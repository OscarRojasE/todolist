$(function () {
    var APPLICATION_ID = "318785BC-F358-0E10-FF42-FE8067055000",
        SECRET_KEY = "38609FC0-808B-113E-FF94-5BD674F09400",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var taskScript = $("#ToDo-Template").html();
    var taskTemplate = Handlebars.compile(taskScript);
    var taskHTML = taskTemplate(wrapper);
    
    $('.main-container').html(taskHTML);
    
});

function Posts(args){
    args = args || "";
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}